pragma Singleton
import QtQuick 2.4
import QuickFlux 1.1

KeyTable {
    property string setCurrentUser
    property string clearCurrentUser
    property string setCurrentUserById
}
